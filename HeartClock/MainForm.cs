﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace HeartClock
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            int baseRadius = ClientSize.Width > ClientSize.Height ? ClientSize.Height *35/ 100 : ClientSize.Width *35/ 100;
            Graphics gr = e.Graphics;
            DrawHeart(sender, e);
            DrawArrow(Color.Yellow, 1, gr, baseRadius - 30, DateTime.Now.Second * 6);
            DrawArrow(Color.Blue, 3, gr, baseRadius - 30, DateTime.Now.Minute * 6 + DateTime.Now.Second/10);
            DrawArrow(Color.Green, 4, gr, baseRadius - 50, DateTime.Now.Hour * 30 + DateTime.Now.Minute / 2 + DateTime.Now.Second / 120);
            DrawScale(Color.Black, 1, gr, baseRadius - 30, 10);

        }

        private void DrawHeart(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            GraphicsPath gp = new GraphicsPath();
            gp.AddBezier(
                new Point(ClientSize.Width / 2, ClientSize.Height / 3),
                new Point(ClientSize.Width/6, 0),
                new Point(0, ClientSize.Height * 2 / 3),
                new Point(ClientSize.Width / 2, ClientSize.Height)
                );

            gp.AddBezier(
               new Point(ClientSize.Width / 2, ClientSize.Height),
               new Point(ClientSize.Width, ClientSize.Height * 2 / 3),
               new Point(ClientSize.Width*5/6, 0),
               new Point(ClientSize.Width / 2, ClientSize.Height / 3)
               );
            PathGradientBrush br = new PathGradientBrush(gp);
            br.CenterPoint = new PointF(ClientSize.Width / 2, ClientSize.Height / 2);
            br.CenterColor = Color.White;
            br.SurroundColors = new Color[]
            {
                Color.Red
            };

            gr.FillPath(br, gp);
        }

        private void DrawScale(Color color, int penWidth, Graphics gr, int radius, int length)
        {
            for (int i = 0; i < 60; ++i)
            {
                GraphicsContainer container =
                                gr.BeginContainer(
                                    new Rectangle(ClientSize.Width / 2, ClientSize.Height *64/ 100, ClientSize.Width, ClientSize.Height),
                                    new Rectangle(0, 0, ClientSize.Width, ClientSize.Height),
                                    GraphicsUnit.Pixel);
                gr.RotateTransform(i * 6);

                gr.DrawLine(new Pen(color, i % 5 == 0 ? penWidth + penWidth : penWidth),
                    new Point(0, i % 5 == 0 ? -radius + length + length : -radius + length),
                    new Point(0, -radius));
                gr.EndContainer(container);
            }
        }

        private void DrawArrow(Color color, int penWidth, Graphics gr, int length, int angle)
        {
            GraphicsContainer container =
                            gr.BeginContainer(
                                new Rectangle(ClientSize.Width / 2, ClientSize.Height * 64 / 100, ClientSize.Width, ClientSize.Height),
                                new Rectangle(0, 0, ClientSize.Width, ClientSize.Height),
                                GraphicsUnit.Pixel);
            gr.RotateTransform(angle);

            gr.DrawLine(new Pen(color, penWidth),
                new Point(0, 0),
                new Point(0, -length)
            );
            gr.EndContainer(container);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void clockTimer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}

